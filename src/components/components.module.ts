import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from 'ionic-angular';

import { FooterComponent } from './footer/footer';

@NgModule({
	declarations: [
		FooterComponent
	],
	imports: [
    FormsModule,
    IonicModule
  ],
	exports: [
		FooterComponent
	]
})
export class ComponentsModule { }
