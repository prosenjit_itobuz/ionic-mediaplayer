import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import 'rxjs/add/operator/takeWhile';

import { PlayerProvider } from '../../providers/audio/audio';

@Component({
  selector: 'app-footer',
  templateUrl: 'footer.html'
})
export class FooterComponent implements OnInit {
  seek: number;
  playState: string = 'pause';
  currentPos: number;
  
  constructor(
    private playerProvider: PlayerProvider,
    private changeDetector: ChangeDetectorRef
  ) {
    
  }

  ngOnInit() {
    let lastval = 0;
   this.playerProvider.progress
   .filter(value => value !== lastval)
   .subscribe(value => {
     if (lastval !== value && !isNaN(value) || value > 0) {
      lastval = value;
      this.currentPos = value;
      this.changeDetector.detectChanges();
     }
   });

   this.playerProvider.playerState.subscribe(state => this.playState = state);
  }

  onseek(value) {
    this.playerProvider.seekRequest.next(value);
  }

  changeState(state) {
    this.playerProvider.changePlayState.next(state);
  }

}
