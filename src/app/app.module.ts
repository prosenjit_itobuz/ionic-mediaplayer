import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { ComponentsModule } from '../components/components.module';
import { TechHome } from './app.component';
import { DirectivesModule } from '../directives/directives.module';
import { StorageProvider } from '../providers/storage/storage';
import { PlayerProvider } from '../providers/audio/audio';

@NgModule({
  declarations: [
    TechHome
  ],
  imports: [
    BrowserModule,
    DirectivesModule,
    ComponentsModule,
    IonicModule.forRoot(TechHome),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    TechHome
  ],
  exports: [
    ComponentsModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    StorageProvider,
    PlayerProvider,
  ],
})
export class AppModule {}
