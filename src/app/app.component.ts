import { Component, ViewChild, OnInit } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import 'rxjs/add/operator/filter';



interface Pages  {
  title: string;
  className: string
};

@Component({
  templateUrl: 'app.html'
})
export class TechHome implements OnInit {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = 'VideoPage';
  pages: Array<Pages> = [
    {title: 'Video', className: 'VideoPage'},
  ];

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen
  ) {
  }

  ngOnInit() {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    this.nav.setRoot(page);
  }

}
