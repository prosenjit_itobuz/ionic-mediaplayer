import { NgModule } from '@angular/core';
import { HttpModule, JsonpModule } from '@angular/http';
import { IonicPageModule } from 'ionic-angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DirectivesModule } from '../../directives/directives.module';
import { SearchPage } from './search';
import { YoutubeProvider } from '../../providers/youtube/youtube';

@NgModule({
  declarations: [
    SearchPage
  ],
  imports: [
    FormsModule,
    HttpModule,
    JsonpModule,
    ReactiveFormsModule,
    DirectivesModule,
    IonicPageModule.forChild(SearchPage)
  ],
  providers: [
    YoutubeProvider
  ]
})
export class SearchPageModule {}
