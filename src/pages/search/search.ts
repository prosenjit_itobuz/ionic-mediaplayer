import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { YoutubeProvider } from '../../providers/youtube/youtube';
import 'rxjs/add/operator/debounceTime';


@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  searchQuery: string = '';
  searchItems: string[];
  searchform: FormGroup;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fb: FormBuilder,
    private yt: YoutubeProvider
  ) {

    this.searchform = this.fb.group({
      q: ['', Validators.required]
    })
  }

  ionViewDidLoad() {
    this.searchform.controls['q'].valueChanges
      .debounceTime(500)
      .subscribe(query => {
       this._autocomplete(query);
      })
  }

  searchText(value) {
    const payload = {
      q: value
    };
    console.log(value);
    this.navCtrl.push('VideoPage', {query: payload})
  }

  search(value) {
    console.log(value);
    this.navCtrl.push('VideoPage', {query: this.searchform.value})
  }

  private _autocomplete(value) {
    this.yt.autocomplete(value)
      .subscribe(res => {
        console.log(res);
        this.searchItems = res[1]
      }, err => {
        console.log(err);
      })
  }

  
}
