import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VideoDetailsPage } from './video-details';
import { HttpModule, JsonpModule } from '@angular/http';

import { ComponentsModule } from '../../components/components.module';
import { YoutubeProvider } from '../../providers/youtube/youtube';
import { NativeStorage } from '@ionic-native/native-storage';
import { IonicAudioModule, AudioProvider, WebAudioProvider, defaultAudioProviderFactory } from 'ionic-audio';


@NgModule({
  declarations: [
    VideoDetailsPage,
  ],
  imports: [
    HttpModule,
    JsonpModule,
    IonicPageModule.forChild(VideoDetailsPage),
    ComponentsModule,
    IonicAudioModule
  ],
  exports: [
    VideoDetailsPage
  ],
  providers: [
    NativeStorage,
    YoutubeProvider
  ]
})
export class VideoDetailsPageModule {}
