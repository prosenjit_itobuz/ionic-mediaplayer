declare var YoutubeVideoPlayer;
declare var window;
import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import {
  Platform, AlertController,
  IonicPage, NavController, NavParams,
  LoadingController
} from 'ionic-angular';
import 'rxjs/add/operator/timeInterval';
import 'rxjs/add/operator/filter';

import { YoutubeProvider } from '../../providers/youtube/youtube';



@IonicPage()
@Component({
  selector: 'page-video-details',
  templateUrl: 'video-details.html',
})
export class VideoDetailsPage {
  video;
  safeURL;
  related;
  iframe;
  formats;
  downPercent;
  dext;
  downloadurl;
  player;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public _sanitizer: DomSanitizer,
    private _yt: YoutubeProvider,
    private _platform: Platform,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController
  ) { }

  ionViewDidLoad() {
    
    let data = this.navParams.get('video');
    if (data) {
      localStorage.setItem('tech_video_dt', JSON.stringify(data));
    } else {
      data = JSON.parse(localStorage.getItem('tech_video_dt'));
    }
    console.log(data);
    if (data) {
      this.video = data;
      const url = `https://www.youtube.com/embed/${data.id.videoId}?&rel=0`;
      this.safeURL = this._sanitizer.bypassSecurityTrustResourceUrl(url);
      this.getRelated(data.id.videoId);
      // this.getFormats(data.id.videoId);
    }
    if (this._platform.is('android')) {
      this.iframe = false;
    } else {
      this.iframe = true;
    }

  }
  viewWillUnload() {
    if (this.player && !this.player.paused) {
      this.player.pause();
    }
    if (this.player) {
      this.player = null;
    }
  }


  ionViewdidLeave() {
    this.navCtrl.pop();
  }

  goToDetails(data) {
    console.log(data);
    if (this.navCtrl.canGoBack()) {
      this.navCtrl.pop();
    }
    this.navCtrl.push('VideoDetailsPage', { video: data });
  }

  getRelated(id) {
    const query = {
      relatedToVideoId: id,
      type: 'video'
    }
    this._yt.search(query)
      .subscribe(related => {
        this.related = related.items;
        console.log('related', related);
      }, err => {
        console.log(err);
      })
  }


  open() {
    if (typeof YoutubeVideoPlayer !== 'undefined') {
      YoutubeVideoPlayer.openVideo(this.video.id.videoId, function (result) {console.log('YoutubeVideoPlayer result = ' + result); });
    }else {
      this.iframe = true;
    }
  }

  getFormats(id) {
    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();
    this._yt.getFormats(id)
      .filter(res => res !== null)
      .subscribe(res => {
        loader.dismiss()
        //  res[res.length - 1].url;
        // this.allTracks = this._audioProvider.tracks; 
        this.downloadurl = res[res.length - 1].url;
        this.player = document.querySelector('audio');
        setTimeout(() => this.player.play(), 2000);
      }, err => {
        console.log(err);
        if (err['status'] === 0) {
          let alert = this.alertCtrl.create();
          alert.setTitle('Service unavailable');
          alert.present();
        }
        loader.dismiss();
      });
  }

  download() {
    window.open(this.downloadurl)
  }

}
