import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HttpModule, JsonpModule } from '@angular/http';

import { DirectivesModule } from '../../directives/directives.module';
import { ComponentsModule } from '../../components/components.module';

import { VideoPage } from './video';
import { YoutubeProvider } from '../../providers/youtube/youtube';
import { FormatkindPipe } from '../../pipes/formatkind/formatkind';

@NgModule({
  declarations: [
    VideoPage,
    FormatkindPipe
  ],
  imports: [
    HttpModule,
    JsonpModule,
    ComponentsModule,
    DirectivesModule,
    IonicPageModule.forChild(VideoPage),
  ],
  providers: [
    YoutubeProvider
  ]
})
export class VideoPageModule {}
