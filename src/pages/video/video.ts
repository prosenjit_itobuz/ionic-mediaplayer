import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { YoutubeProvider } from '../../providers/youtube/youtube';

@IonicPage()
@Component({
  selector: 'page-video',
  templateUrl: 'video.html',
})
export class VideoPage {
  videos;

  constructor(
   public navCtrl: NavController,
   public navParams: NavParams,
   private _youtube: YoutubeProvider,
   ) {}

  ionViewDidLoad() {
    const query = this.navParams.get('query');
    if(query) {
      this.getSearchResult(query);
    }else {
      const lastquery = this._youtube.getLastQuery();
      this.getSearchResult(lastquery);
    }
  }

  getSearchResult(data) {
    this._youtube.search(data)
      .subscribe(result => {
        console.log(result);
        this.videos = result.items;
      }, err => {
        console.log(err);
      })
  }

  goToDetails(data) {
    this.navCtrl.push('VideoDetailsPage', {video: data});
  }

}
