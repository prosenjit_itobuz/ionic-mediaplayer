import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatkind',
})
export class FormatkindPipe implements PipeTransform {
  transform(value: string, ...args) {
    const kind = value.split('#');
    return kind[1];
  }
}
