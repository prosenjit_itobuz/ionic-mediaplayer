export interface VideoList {
  data: Array<string>
};

export interface Video {
  id: string,
  title: string,
  ext: string
};