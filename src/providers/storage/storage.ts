import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


@Injectable()
export class StorageProvider {

  private videolist: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  videolist$ = this.videolist.asObservable();

  private downloadQueList: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  downloadQueList$ = this.downloadQueList.asObservable();


  constructor(
  ) {}

  /**
   * Get data list
   * @param datatype
   */
  getDataList(datatype: string) {
    const temp = localStorage.getItem('tech_' + datatype) ? JSON.parse(localStorage.getItem('tech_' + datatype)) : [];
    console.log(temp);
    this[datatype].next(temp);
  }

  getData(datatype: string) {
    const temp = localStorage.getItem('tech_' + datatype) ? JSON.parse(localStorage.getItem('tech_' + datatype)) : [];
    this[datatype].next(temp);
    return temp;
  }

  set(datatype: string, data: any) {
    let dataListTemp = localStorage.getItem('tech_' + datatype) ? JSON.parse(localStorage.getItem('tech_' + datatype)) : [];
    let index = -1;
    if (dataListTemp) {
      index = dataListTemp.findIndex(item => {
        return item.id === data.id;
      })
    }
    

    // update data
    if (index === -1) {
      dataListTemp.push(data);
      localStorage.setItem('tech_'+datatype, JSON.stringify(dataListTemp));
      this[datatype].next(dataListTemp);
      console.log('data updated', dataListTemp);
    }
  }

  removeItem(datatype: string, id: string) {
    let dataListTemp = localStorage.getItem('tech_' + datatype) ? JSON.parse(localStorage.getItem('tech_' + datatype)) : [];
    let index = -1;
    
    if (dataListTemp) {
      index = dataListTemp.findIndex(item => {
        return item.id === id;
      })
    }else {
      dataListTemp = [];
    }
    console.log(index, dataListTemp);

    if (index !== -1) {
      dataListTemp.splice(index, 1);
      this[datatype].next(dataListTemp);
      localStorage.setItem('tech_'+datatype, JSON.stringify(dataListTemp));
    }
  }

  getItem(datatype: string, id: string) {
    let dataListTemp = localStorage.getItem('tech_' + datatype) ? JSON.parse(localStorage.getItem('tech_' + datatype)) : [];
    let index = -1;
    if (dataListTemp) {
      index = dataListTemp.findIndex(item => {
        return item.id === id;
      })
    }

    if (index !== -1) {
      return dataListTemp[index];
    }
  }

  /**
   * Get mime type for file opener
   * @param ext
   */
  getMime(ext?) {
    const mimes = {
      'mp3': 'audio/mpeg',
      'mp4': 'video/mp4',
      'mpeg': 'video/mpeg',
      'avi': 'video/x-msvideo',
      'wav': 'audio/x-wav',
      'm4a': 'audio/m4a',
    }
    return mimes[ext];
  }

  /**
   * Only show download format in this list
   */
  downloadextlist() {
    return [
      'mp3',
      'mp4',
      'mpeg',
      'avi',
      'wav',
      'm4a'
    ]
  }

}
