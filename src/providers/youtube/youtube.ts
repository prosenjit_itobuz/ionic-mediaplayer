import { Injectable } from '@angular/core';
import { Http, URLSearchParams, Jsonp } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

const KEY = 'AIzaSyBh7Kmt6Ql9Ih4_7MkbwQ-iys8NG2hiBUk';
const API= 'https://www.googleapis.com/youtube/v3'

@Injectable()
export class YoutubeProvider {

  constructor(
    public http: Http,
    private jsonp: Jsonp
    ) {
  }

  getLastQuery() {
    const query = localStorage.getItem('tech_video_query')? JSON.parse(localStorage.getItem('tech_video_query')) : null;
    return query;
  }

  search(query): Observable<any> {
    console.log(query);
    const queryParams = new URLSearchParams();
    for (let key in query) {
      queryParams.set(key, query[key]);
    }
    localStorage.setItem('tech_video_query', JSON.stringify(query));
    const url = `${API}/search?${queryParams}&type=video&safeSearch=none&part=snippet&maxResults=25&key=${KEY}`;
    
    return this.http.get(url)
      .map(result => {
        return result.json();
      })
      .catch(err => this.handleError(err));
  }

  autocomplete(query) {
    const url = `https://suggestqueries.google.com/complete/search?client=youtube&ds=yt&client=firefox&q=${query}&callback=JSONP_CALLBACK`;

    return this.jsonp.request(url)
      .map(res => res.json())
      .catch(err => this.handleError(err));
  }

  getFormats(id) {
    const url = `https://web2developers.in/video/${id}`;
    return this.http.get(url)
      .map(res => {
        console.log(res);
        return res.json();
      })
      .catch(err => this.handleError(err));
  }

  private handleError(error: Response | any) {
    console.log(error);
    return Observable.throw(error);
  }

}

