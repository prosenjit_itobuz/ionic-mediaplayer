import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class PlayerProvider {
  onPlay: Subject<boolean> = new Subject();
  mediaSrc: Subject<string> = new Subject();
  seekRequest: Subject<number> = new Subject();
  progress: Subject<number> = new Subject();
  playerState: Subject<string> = new Subject();
  changePlayState: Subject<string> = new Subject();

  constructor() {
  }

}
